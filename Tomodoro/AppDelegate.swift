//
//  AppDelegate.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 15.04.21.
//

import Cocoa
import Starscream
import Combine
import UserNotifications

@main
class AppDelegate: NSObject, NSApplicationDelegate, WebSocketDelegate, UNUserNotificationCenterDelegate, PopoverViewControllerDelegate {
    private var isRunning = false
    private var currentPhase = Phase.Verfügbar
    private var fetching = false
    
    private var popover = NSPopover()
    private var popoverViewController: PopoverViewController?
    
    private var subscription = Set<AnyCancellable>()
    
    private var socket: WebSocket!
    private var statusBarItem: NSStatusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.variableLength)
    private let apiClient = TomodoroAPIClient()
    private var timer: Timer?
    private var remainingTimerTime = 0
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        NSApp.setActivationPolicy(.accessory)
        NSApplication.shared.setActivationPolicy(.accessory)
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .badge, .sound], completionHandler: {(granted, error) in })
        
        registerCategories()
        
        guard let statusButton = statusBarItem.button else { return }
        statusButton.action = #selector(togglePopover(_sender:))
        popoverViewController = PopoverViewController.freshController()
        popoverViewController?.delegate = self
        
        popover.appearance = NSAppearance(named: .darkAqua)
        popover.behavior = NSPopover.Behavior.transient;
        popover.contentViewController = popoverViewController
        
        statusButton.title = "00:00"
        
        UserDefaults.standard
            .publisher(for: \.team)
            .handleEvents(receiveOutput: { team in
                if team == nil {
                    UserDefaults.standard.team = NamesGenerator.getRandomName()
                    return
                }
                
                if let team = team  {
                    self.apiClient.send(GetTeam(team: team)) { response in
                        switch response {
                        case .success(let teamResponse):
                            UserDefaults.standard.focus = teamResponse.settings.focus
                            UserDefaults.standard.pause = teamResponse.settings.pause

                            if let timer = teamResponse.timer {
                                self.isRunning = true
                                self.currentPhase = timer.name
                            } else {
                                self.isRunning = false
                                self.currentPhase = .None
                            }

                            self.popoverViewController?.team = team
                            self.connect(team: teamResponse.name)
                        case .failure(let error):
                            print(error)
                            self.apiClient.post(CreateTeam(team: team)) { response in
                                
                            }
                        }
                    }
                    
                }
            })
            .sink { _ in }
            .store(in: &subscription)
    }
    
    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
    
    func didReceive(event: WebSocketEvent, client: WebSocket) {
        guard let statusButton = statusBarItem.button else { return }
        switch event {
        case .connected(_):
            popoverViewController?.connected = true
            statusButton.title = "00:00"
        case .disconnected(_, _):
            popoverViewController?.connected = false
        case .text(let string):
            handleMessage(text: string)
        case .binary(_):
            break
        case .pong(_):
            break
        case .ping(_):
            break
        case .error(let error):
            popoverViewController?.connected = false
            handleError(error)
        case .viabilityChanged(_):
            break
        case .reconnectSuggested(_):
            break
        case .cancelled:
            break
        }
    }
    
    func handleError(_ error: Error?) {
        if let e = error as? WSError {
            print("Websocket encountered an error: \(e.message)")
        } else if let e = error {
            print("Websocket encountered an error: \(e.localizedDescription)")
        } else {
            print("Websocket encountered an error")
        }
    }
    
    private func connect(team: String) {
        if socket != nil {
            socket.disconnect()
        }
        
        let url = URL(string: "wss://api.tomodoro.de/api/v1/team/\(UserDefaults.standard.team!)/ws")!
        var request = URLRequest(url: url)
        request.timeoutInterval = 5
        
        socket = WebSocket(request: request)
        socket.delegate = self
        socket.connect()
    }
    
    private func handleMessage(text: String) {
        guard let statusButton = statusBarItem.button else { return }
        statusButton.imagePosition = .imageLeft
        let message: Message = try! JSONDecoder().decode(Message.self, from: text.data(using: .utf8)!)
        switch message.type {
        case .timerStarted:
            isRunning = true
            if popover.isShown {
                popoverViewController?.isRunning = true
            }
        
            let timerStartedMessage = try! JSONDecoder().decode(TimerStarted.self, from: text.data(using: .utf8)!)
            
            let center = UNUserNotificationCenter.current()
            let content = UNMutableNotificationContent()
            
            currentPhase = timerStartedMessage.payload.name
            
            if timerStartedMessage.payload.name == Phase.Fokusphase {
                statusButton.image = NSImage(named: NSImage.statusUnavailableName)
                content.title = "Fokusphase gestartet"
                content.categoryIdentifier = "focusStarted"
            } else {
                statusButton.image = NSImage(named: NSImage.statusAvailableName)
                content.title = "Pause gestartet"
                content.categoryIdentifier = "pauseStarted"
            }
            
            let (_, min, sec) = timerStartedMessage.getFormatedDuratoin();
            let endDate = timerStartedMessage.getEndTime()
            let dateFormater = DateFormatter()
            dateFormater.dateFormat = "HH:mm"
            let endTime = dateFormater.string(from: endDate)
        
            var body = String(format: "Dauer: %0d\tMinuten Ende: \(endTime)", min)
            
            if min == 0 {
                body = String(format: "Dauer: %0d\tSekunden Ende: \(endTime)", sec)
            }
            
            content.body = body
            content.sound = UNNotificationSound.default
            
            let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: .none)
            center.add(request)
            
            remainingTimerTime = Int(timerStartedMessage.payload.duration / 1000000 / 1000 * 2)
            
            timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(tick), userInfo: nil, repeats: true)
            
            break
        case .timerStopped:
            isRunning = false
            if popover.isShown {
                popoverViewController?.isRunning = false
            }
            
            let center = UNUserNotificationCenter.current()
            let content = UNMutableNotificationContent()
            
            content.title = "Tomodoro abgeschlossen"
            content.sound = UNNotificationSound.default
            content.categoryIdentifier = "timerStopped"
            
            let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: .none)
            center.add(request)
            
            statusButton.title = "00:00"
            statusButton.image = nil
            
            popoverViewController?.remainingTime = "00:00"
            popoverViewController?.phase = .None
            popoverViewController?.remainingSeconds = 0
            popoverViewController?.isRunning = false
            
            if let timer = timer {
                remainingTimerTime = 0
                timer.invalidate()
            }
            
            break
        case .tick:
            isRunning = true
        
            let tickMessage = try! JSONDecoder().decode(Tick.self, from: text.data(using: .utf8)!)//
            currentPhase = tickMessage.payload.name
            
            if remainingTimerTime == 0 {
                remainingTimerTime = Int(tickMessage.payload.remainingTime / 1000000 / 1000 * 2)
                timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(tick), userInfo: nil, repeats: true)
            }
            
            if tickMessage.payload.name == Phase.Fokusphase {
                statusButton.image = NSImage(named: NSImage.statusUnavailableName)
            } else {
                statusButton.image = NSImage(named: NSImage.statusAvailableName)                
            }
            
            break
        }
    }
    
    private func startTimer(team: String, phase: Phase, duration: UInt64) {
        if !fetching {
            fetching = true
            apiClient.put(StartTimer(team: team, name: phase.rawValue, duration: duration)) { _ in  self.fetching = false}
        }
    }
    
    private func stopTimer(team: String, completionHandler: @escaping () -> Void ) {
        if !fetching {
            fetching = true
            apiClient.delete(StopTimer(team: team)) { _ in
                self.fetching = false
                completionHandler()
            }
        }
    }
    
    private func registerCategories() {
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        
        let stopFocus = UNNotificationAction(identifier: "stopFocus", title: "Stopp Focus", options: .foreground)
        let stopPause = UNNotificationAction(identifier: "stopPause", title: "Stopp Pause", options: .foreground)
        let startFocus = UNNotificationAction(identifier: "startFocus", title: "Start Focus", options: .foreground)
        let startPause = UNNotificationAction(identifier: "startPause", title: "Start Pause", options: .foreground)
        
        let focusStarted = UNNotificationCategory(identifier: "focusStarted", actions: [stopFocus, startPause], intentIdentifiers: [])
        let pauseStarted = UNNotificationCategory(identifier: "pauseStarted", actions: [stopPause, startFocus], intentIdentifiers: [])
        let timerStopped = UNNotificationCategory(identifier: "timerStopped", actions: [startFocus, startPause], intentIdentifiers: [])
        
        center.setNotificationCategories([focusStarted, pauseStarted, timerStopped])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        switch response.actionIdentifier {
        case UNNotificationDefaultActionIdentifier:
            print("Default identifier")
        
        case "stopFocus", "stopPause":
            let team = UserDefaults.standard.team!
            
            stopTimer(team: team) {}
            isRunning = false
            
        case "startFocus":
            let team = UserDefaults.standard.team!
            let duration = UserDefaults.standard.focus
            
            if isRunning {
                stopTimer(team: team) {}
                isRunning = false
            }
            
            startTimer(team: team, phase: Phase.Fokusphase, duration: duration)
        
        case "startPause":
            let team = UserDefaults.standard.team!
            let duration = UserDefaults.standard.pause
            
            if isRunning {
                stopTimer(team: team) {}
                isRunning = false
            }
            
            startTimer(team: team, phase: Phase.Verfügbar, duration: duration)
            
        default:
            break
        }
    }
    
    @objc func togglePopover(_sender: Any?) {
        if popover.isShown {
            popover.performClose(_sender)
        } else {
            if let button = statusBarItem.button {
                popover.show(relativeTo: button.bounds, of: button, preferredEdge: NSRectEdge.minY)
            }
        }
    }
    
    func stopTimer(_ popoverViewController: PopoverViewController) {
        let team = UserDefaults.standard.team!
        stopTimer(team: team) {}
    }
    
    func resetWebsocket(_ popoverViewController: PopoverViewController) {
        connect(team: UserDefaults.standard.team!)
    }
    
    func startPhase(_ popoverViewController: PopoverViewController, phase: Phase) {
        let team = UserDefaults.standard.team!
        
        if isRunning {
            stopTimer(team: team) {
                if phase == .Fokusphase {
                    let duration = UserDefaults.standard.focus
                    self.startTimer(team: team, phase: phase, duration: duration)
                } else {
                    let duration = UserDefaults.standard.pause
                    self.startTimer(team: team, phase: phase, duration: duration)
                }
            }
        }
        
        if phase == .Fokusphase {
            let duration = UserDefaults.standard.focus
            startTimer(team: team, phase: phase, duration: duration)
        } else {
            let duration = UserDefaults.standard.pause
            startTimer(team: team, phase: phase, duration: duration)
        }
    }
    
    @objc func tick() {
        if remainingTimerTime > 0 {
            remainingTimerTime -= 1
            
            let duration = remainingTimerTime / 2
            let minutes = duration / 60 % 60
            let seconds = duration % 60
            guard let statusButton = statusBarItem.button else { return }
            
            statusButton.title = String(format: "%02d:%02d", minutes, seconds)
            
            if popover.isShown {
                let popoverViewController = (popover.contentViewController as! PopoverViewController)
                
                popoverViewController.remainingTime = String(format: "%02d:%02d", minutes, seconds)
                popoverViewController.phase = currentPhase
                popoverViewController.remainingSeconds = duration
                popoverViewController.isRunning = true
            }
            
        } else {
            guard let timer = timer else { return }
            timer.invalidate()
        }
    }
}

