//
//  APIRequest.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 28.04.21.
//

import Foundation

public protocol APIRequest: Encodable {
    
    associatedtype Response: Decodable
    associatedtype NotFoundResponse: Decodable
    
    var resourceName: String { get }
}
