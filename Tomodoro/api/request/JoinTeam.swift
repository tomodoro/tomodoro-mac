//
//  JoinTeam.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 13.05.21.
//

import Foundation

public struct JoinTeam: APIRequest {
    public typealias Response = GetTeamResponse
    public typealias NotFoundResponse = EmptyResponse
    
    public var resourceName: String {
        return "team/\(team)/join"
    }
    
    private let team: String
    let client: UUID
    
    init(team: String, client: UUID) {
        self.team = team
        self.client = client
    }
}
