//
//  StopTimer.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 30.04.21.
//

import Foundation

public struct StopTimer: APIRequest {
    public typealias Response = APIMessage
    public typealias NotFoundResponse = EmptyResponse
    
    public var resourceName: String {
        return "team/\(team)/timer"
    }
    
    let team: String
    
    init(team: String) {
        self.team = team
    }
    
}
