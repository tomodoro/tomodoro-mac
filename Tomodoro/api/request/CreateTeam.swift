//
//  CreateTeam.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 14.05.21.
//

import Foundation

public struct CreateTeam: APIRequest {
    
    public typealias Response = CreateTeamResponse
    public typealias NotFoundResponse = EmptyResponse
    
    public var resourceName: String {
        return "team"
    }
    
    private let team: String
    
    public init(team: String) {
        self.team = team
    }
}
