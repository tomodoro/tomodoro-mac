//
//  StartTimer.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 29.04.21.
//

import Foundation

public struct StartTimer: APIRequest {
    public typealias Response = StartTimerResponse
    public typealias NotFoundResponse = EmptyResponse
    
    public var resourceName: String {
        return "team/\(team)/timer/start"
    }
    
    let team: String
    let name: String
    let duration: UInt64
    
    init(team: String, name: String, duration: UInt64) {
        self.team = team
        self.name = name
        self.duration = duration
    }
    
}
