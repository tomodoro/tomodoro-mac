//
//  GetTeam.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 28.04.21.
//

import Foundation

public struct GetTeam: APIRequest {
    
    public typealias Response = GetTeamResponse
    public typealias NotFoundResponse = TeamNotFoundResponse
    
    public var resourceName: String {
        return "team/\(team)"
    }
    
    private let team: String
    
    public init(team: String) {
        self.team = team
    }
}
