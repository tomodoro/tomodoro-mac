//
//  CreateTeamResponse=.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 14.05.21.
//

import Foundation

public struct CreateTeamResponse: Decodable {
    
    public let name: String
}
