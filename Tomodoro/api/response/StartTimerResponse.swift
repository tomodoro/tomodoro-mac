//
//  StartTimerResponse.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 30.04.21.
//

import Foundation

public struct StartTimerResponse: Decodable {
    
    public let href: String
    public let timer: TomodoroTimer
}
