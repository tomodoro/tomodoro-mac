//
//  EmptyResponse.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 13.05.21.
//

import Foundation

public struct EmptyResponse: Decodable {}
