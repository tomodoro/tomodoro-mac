//
//  APIResponse.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 30.04.21.
//

import Foundation

public protocol APIResponse: Decodable {
    var href: String { get }
}
