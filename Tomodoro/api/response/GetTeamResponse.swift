//
//  GetTeamResponse.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 30.04.21.
//

import Foundation

public struct GetTeamResponse: Decodable {
    
    public let href: String
    public let name: String
    public let links: [Link]
    public let timer: TomodoroTimer?
    public let settings: Settings
}
