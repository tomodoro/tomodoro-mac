//
//  TomodoroResponse.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 28.04.21.
//

import Foundation

public struct TomodoroResponse<Response: Decodable>:Decodable {
    public let status: String?
    
    public let message: String?
    
    public let data: Response?
}
