//
//  Message.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 30.04.21.
//

import Foundation

public struct APIMessage: Decodable {
    public let href: String
    public let Message: String
    public let types: String
}
