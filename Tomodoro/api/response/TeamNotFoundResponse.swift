//
//  TeamNotFoundResponse.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 13.05.21.
//

import Foundation

public struct TeamNotFoundResponse: Decodable {
    
    public let href: String
    public let error: APIError
}
