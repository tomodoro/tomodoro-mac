//
//  TomodoroAPIClient.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 28.04.21.
//

import Foundation

public typealias ResultCallback<Value> = (Result<Value, Error>) -> Void

public class TomodoroAPIClient {
    private let baseEndpointUrl = URL(string: "https://api.tomodoro.de/api/v1/")!
    private let session = URLSession(configuration: .default)
    
    public func send<T: APIRequest>(_ request: T, completion: @escaping ResultCallback<T.Response>) {
        let endpoint = self.endpoint(for: request)
        let request = URLRequest(url: endpoint)
    
        let task = session.dataTask(with: request) { data, response, error in
            guard let data = data, let httpResponse = response as? HTTPURLResponse, error == nil else {
                print("No valid response")
                completion(.failure(error!))
                return
            }
            
            guard 200 ..< 300 ~= httpResponse.statusCode else {
                if 404 == httpResponse.statusCode {
                    do {
                        let responseBody = try JSONDecoder().decode(T.NotFoundResponse.self, from: data)
                        completion(.failure(NSError(domain: "", code: httpResponse.statusCode, userInfo: ["errorResponse": responseBody])))
                        return
                    } catch {}
                }
                completion(.failure(NSError(domain: "", code: httpResponse.statusCode, userInfo: ["message": "Status code was \(httpResponse.statusCode), but expected 2xx"])))
                return
            }
            
            do {
                let responseBody = try JSONDecoder().decode(T.Response.self, from: data)
                completion(.success(responseBody))
            } catch {
                completion(.failure(error))
            }
        }
        task.resume()
    }
    
    public func post<T: APIRequest>(_ request: T, completion: @escaping ResultCallback<T.Response> ) {
        return modifiy(request, method: "POST", completion: completion)
    }
    
    public func put<T: APIRequest>(_ request: T, completion: @escaping ResultCallback<T.Response>) {
        return modifiy(request, method: "PUT", completion: completion)
    }
    
    private func modifiy<T: APIRequest>(_ request: T, method: String, completion: @escaping ResultCallback<T.Response>) {
        do {
            let endpoint = self.endpoint(for: request)
            
            let encodedData = try JSONEncoder().encode(request)
            
            var request = URLRequest(url: endpoint)
            request.httpMethod = method
            request.httpBody = encodedData
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let task = session.dataTask(with: request) { data, response, error in
                guard let data = data, let httpResponse = response as? HTTPURLResponse, error == nil else {
                    print("No valid response")
                    completion(.failure(error!))
                    return
                }
                
                guard 200 ..< 300 ~= httpResponse.statusCode else {
                    completion(.failure(NSError(domain: "", code: httpResponse.statusCode, userInfo: ["message": "Status code was \(httpResponse.statusCode), but expected 2xx"])))
                    return
                }
                
                do {
                    let responseBody = try JSONDecoder().decode(T.Response.self, from: data)
                    completion(.success(responseBody))
                } catch {
                    completion(.failure(error))
                }
            }
            task.resume()
        } catch {
            completion(.failure(error))
        }
    }
    
    public func delete<T: APIRequest>(_ request: T, completion: @escaping ResultCallback<T.Response>) {
        let endpoint = self.endpoint(for: request)
        
        var request = URLRequest(url: endpoint)
        request.httpMethod = "DELETE"
        
        let task = session.dataTask(with: request) { data, response, error in
            guard let data = data, let httpResponse = response as? HTTPURLResponse, error == nil else {
                print("No valid response")
                completion(.failure(error!))
                return
            }
            
            guard 410 == httpResponse.statusCode else {
                completion(.failure(NSError(domain: "", code: httpResponse.statusCode, userInfo: ["message": "Status code was \(httpResponse.statusCode), but expected 410 Gone"])))
                return
            }
            
            do {
                let responseBody = try JSONDecoder().decode(T.Response.self, from: data)
                completion(.success(responseBody))
            } catch {
                completion(.failure(error))
            }
        }
        task.resume()
    }
    
    private func endpoint<T: APIRequest>(for request: T) -> URL {
        guard let baseUrl = URL(string: request.resourceName, relativeTo: baseEndpointUrl) else {
            fatalError("Bad resourceName: \(request.resourceName)")
        }
        
        let components = URLComponents(url: baseUrl, resolvingAgainstBaseURL: true)!
        
        return components.url!
    }
}
