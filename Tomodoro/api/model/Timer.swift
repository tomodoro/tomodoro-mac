//
//  Timer.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 29.04.21.
//

import Foundation

public struct TomodoroTimer: Decodable {

    public let duration: UInt64
    public let name: Phase
    public let start:String
}
