//
//  Link.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 29.04.21.
//

import Foundation

public struct Link: Decodable {
    public let link: String
    public let rel: String
    public let type: String
}
