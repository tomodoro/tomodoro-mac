//
//  Error.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 13.05.21.
//

import Foundation

public struct APIError: Decodable {
    
    public let error: Int
    public let message: String
}
