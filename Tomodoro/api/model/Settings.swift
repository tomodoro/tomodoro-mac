//
//  Settings.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 28.04.21.
//

import Foundation

public struct Settings: Decodable {
    public let focus: UInt64
    public let pause: UInt64
}
