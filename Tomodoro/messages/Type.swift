//
//  Type.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 21.04.21.
//

import Foundation

enum Type: String, Decodable {
    case tick, timerStopped, timerStarted
}
