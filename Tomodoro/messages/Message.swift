//
//  Message.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 21.04.21.
//

import Foundation

struct Message: Decodable {
    let type: Type
}
