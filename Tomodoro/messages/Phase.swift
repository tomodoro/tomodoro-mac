//
//  Phase.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 21.04.21.
//

import Foundation

public enum Phase: String, Decodable {
    case Fokusphase, Verfügbar, None
}
