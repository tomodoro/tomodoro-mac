//
//  TimerStarted.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 21.04.21.
//

import Foundation

struct TimerStarted: Decodable {
    let type: Type
    
    struct Payload: Decodable {
        let duration: UInt64
        let name: Phase
        let team: String
        let timestamp: UInt64
    }
    
    let payload: Payload
    
    func getFormatedDuratoin() -> (UInt64, UInt64, UInt64) {
        let duration = self.payload.duration / 1000000 / 1000
        let hours = duration / 60 / 60
        let minutes = duration / 60 % 60
        let seconds = duration % 60
        
        return (hours, minutes, seconds)
    }
    
    func getEndTime() -> Date {
        let calendar = Calendar.current
        return calendar.date(byAdding: .second, value: Int(self.payload.duration / 1000000 / 1000), to: Date.init())!
    }
}
