//
//  TimerStopped.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 21.04.21.
//

import Foundation

struct TimerStopped: Decodable {
    let type: Type
    
    struct Payload: Decodable {
        let team: String
        let timestamp: UInt64
    }
    
    let payload: Payload
}
