//
//  Tick.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 21.04.21.
//

import Foundation

struct Tick: Decodable {
    let type: Type

    struct Payload: Decodable {
        let name: Phase
        let remainingTime: UInt64
        let team: String
        let timestamp: UInt64
    }

    let payload: Payload;

    func getFormatedRemainingTime() -> (UInt64, UInt64, UInt64) {
        let remainingTime = self.payload.remainingTime / 1000000 / 1000
        let hours = remainingTime / 60 / 60
        let minutes = remainingTime / 60 % 60
        let seconds = remainingTime % 60
        
        return(hours, minutes, seconds)
    }
}
