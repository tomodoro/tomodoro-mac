//
//  UserDefaults.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 16.04.21.
//

import Foundation

extension UserDefaults {
    @objc var team: String? {
        get {
            return string(forKey: "team")
        }
        
        set {
            set(newValue, forKey: "team")
        }
    }
    
    @objc var focus: UInt64 {
        get {
            return UInt64(integer(forKey: "focus"))
        }
        
        set {
            set(newValue, forKey: "focus")
        }
    }
    
    @objc var pause: UInt64 {
        get {
            return UInt64(integer(forKey: "pause"))
        }
        
        set {
            set(newValue, forKey: "pause")
        }
    }
}
