//
//  String.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 14.05.21.
//

import Foundation

extension String {
    func kebapcased() -> String {
        return self.lowercased().replacingOccurrences(of: " ", with: "-"
        )
    }
}
