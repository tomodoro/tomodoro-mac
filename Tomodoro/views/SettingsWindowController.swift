//
//  SettingsWindow.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 14.05.21.
//

import Foundation
import Cocoa

public class SettingsWindowController: NSWindowController {
    
    override public func windowDidLoad() {
        NSApp.activate(ignoringOtherApps: true)
        self.window?.makeKeyAndOrderFront(self)
        self.window?.collectionBehavior = .canJoinAllSpaces
        self.window?.level = NSWindow.Level.tornOffMenu
        
    }
}
