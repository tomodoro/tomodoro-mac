//
//  TeamLabel.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 11.05.21.
//

import Foundation
import Cocoa

class TeamLabel: NSTextField {
    var image: NSImage = NSImage(named: NSImage.statusAvailableName)!
        
    override func draw(_ dirtyRect: NSRect) {
        let center = NSPoint(x: NSMidX(dirtyRect), y: NSMidY(dirtyRect))
        
        let teamfont = self.font!
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        let color = self.textColor!
        
        let textRange = (stringValue as NSString).range(of: stringValue)
        let textString = NSMutableAttributedString(string: stringValue)
        
        textString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: textRange)
        textString.addAttribute(NSAttributedString.Key.font, value: teamfont, range: textRange)
        textString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: textRange)
        
        let constraintRect = CGSize(width: CGFloat(0), height: CGFloat(0))
        let boundingBox = textString.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading])
        
        let imageRepresentation = image.representations[0]
        let imageWidth = imageRepresentation.size.width
        
        textString.draw(with: CGRect(x: CGFloat(imageWidth/2), y: CGFloat(0), width: dirtyRect.width, height: boundingBox.height), options: [.usesLineFragmentOrigin,  .usesFontLeading], context: nil)
        
        let x = (center.x - boundingBox.width / 2) - CGFloat(imageWidth/2)
        
        image.draw(at: NSPoint(x: x, y:0), from: NSZeroRect, operation: .sourceOver, fraction: 1)
    }
}
