//
//  SettingsViewController.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 16.04.21.
//

import Cocoa

class SettingsViewController: NSViewController {
    
    @IBOutlet weak var saveButton: NSButton!
    @IBOutlet weak var teamTextField: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.team == nil {
            return
        }
        
        teamTextField.stringValue = UserDefaults.standard.team!
    }
    
    @IBAction func onSave(_ sender: NSButton) {
        UserDefaults.standard.team = teamTextField.stringValue
        self.view.window?.close()
    }
    
    @IBAction func onCancle(_ sender: NSButton) {
        self.view.window?.close()
    }
}
