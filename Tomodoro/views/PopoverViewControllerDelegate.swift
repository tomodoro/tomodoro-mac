//
//  PopoverViewControllerDelegate.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 07.05.21.
//

import Foundation

protocol PopoverViewControllerDelegate: AnyObject {
    
    func resetWebsocket(_ popoverViewController: PopoverViewController)
    func startPhase(_ popoverViewController: PopoverViewController, phase: Phase)
    func stopTimer(_ popoverViewController: PopoverViewController)
}
