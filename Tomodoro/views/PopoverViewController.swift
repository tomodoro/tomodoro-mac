//
//  PopoverViewController.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 04.05.21.
//

import Cocoa
import Sparkle

class PopoverViewController: NSViewController {

    @IBOutlet private weak var leftButton: NSButton!
    @IBOutlet private weak var rightButton: NSButton!
    
    @IBOutlet private weak var reconnectButton: NSButton!
    @IBOutlet private weak var settingsButton: NSButton!
    @IBOutlet private weak var teamLabel: TeamLabel!
    @IBOutlet private weak var timer: TimerView!
    @IBOutlet var settingsMenu: NSMenu!
    @IBOutlet private var versionItem: NSMenuItem!
    
    weak var delegate: PopoverViewControllerDelegate?

    var phase: Phase = .None {
        didSet {
            timer.phase = phase
            
            timer.needsDisplay = true
        }
    }
    
    var isRunning: Bool = false {
        didSet {
            if !isRunning {
                rightButton.title = "Start Pause"
                leftButton.title = "Start Focus"
            } else {
                rightButton.title = "Stop Timer"
                
                if phase == .Fokusphase {
                    leftButton.title = "Start Pause"
                } else {
                    leftButton.title = "Start Focus"
                }
            }
        }
    }
    
    var remainingTime: String = "00:00" {
        didSet {
            timer.remainingTime = remainingTime
        }
    }
    
    var remainingSeconds: Int = 0 {
        didSet {
            timer.remainingSecs = remainingSeconds
        }
    }
    
    var team: String = ""
    
    var connected: Bool = false {
        didSet {
            if teamLabel != nil {
                if connected {
                    teamLabel.image = NSImage(named: NSImage.statusAvailableName)!
                } else {
                    teamLabel.image = NSImage(named: NSImage.statusUnavailableName)!
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingsButton?.toolTip = "Settings"
        reconnectButton?.toolTip = "Reconnect"
        teamLabel.stringValue = team
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let displayName = Bundle.main.infoDictionary?["CFBundleDisplayName"] as? String
        let bundleVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
        
        if appVersion != nil && displayName != nil && bundleVersion != nil {
            versionItem.title = "\(displayName!) v\(appVersion!) (\(bundleVersion!))"
        }
    }
    
    override func viewDidAppear() {
        teamLabel.stringValue = team
    }
    
    static func freshController() -> PopoverViewController {
        let storyboard = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
        
        let identifier = NSStoryboard.SceneIdentifier( "PopoverViewController")
        
        guard let viewcontroller = storyboard.instantiateController(withIdentifier: identifier) as? PopoverViewController else {
            fatalError("Unable to instantiate ViewController in Main.storyboard")
        }
        
        return viewcontroller
    }
    
    @IBAction func leftButtonAction(_ sender: NSButton) {
        var phase = self.phase
        
        if phase == .None || phase == .Verfügbar {
            phase = .Fokusphase
        } else {
            phase = .Verfügbar
        }
        
        delegate?.startPhase(self, phase: phase)
    }
    
    @IBAction func rightButtonAction(_ sender: NSButton) {
        if phase == .None {
            delegate?.startPhase(self, phase: .Verfügbar )
        }
        
        if phase != .None && isRunning {
            delegate?.stopTimer(self)
        }
    }
    
    @IBAction func reconnect(_ sender: NSButton) {
        delegate?.resetWebsocket(self)
    }

    
    @IBAction func showMenu(_ sender: NSButton) {
        let location = NSPoint(x: 0, y: sender.frame.height + 5)
        settingsMenu.popUp(positioning: nil, at: location, in: sender)
    }
    
    @IBAction func quitApplication(_ sender: NSMenuItem) {
        NSApplication.shared.terminate(self)
    }
    
    @IBAction func checkForUpdates(_ sender: Any) {
           let updater = SUUpdater.shared()
           updater?.checkForUpdates(self)
       }
}
