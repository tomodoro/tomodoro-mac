//
//  TimerView.swift
//  Tomodoro
//
//  Created by Patrick Hempel on 06.05.21.
//

import Foundation
import Cocoa

class TimerView: NSView {
    
    var phase: Phase = .None {
        didSet {
            needsDisplay = true
        }
    }
    var remainingSecs: Int = 0 {
        didSet {
            needsDisplay = true
        }
    }
    
    var remainingTime: String = "00:00" {
        didSet {
            needsDisplay = true
        }
    }
    
    override func draw(_ rect: NSRect) {
        let center = NSPoint(x: NSMidX(rect), y: NSMidY(rect))
        
        let backgroundPath = NSBezierPath()
        backgroundPath.appendArc(withCenter: center, radius: rect.width / 2 - 4, startAngle: 0, endAngle: 360, clockwise: false)
        NSColor.lightGray.set()
        backgroundPath.lineCapStyle = .round
        backgroundPath.lineWidth = 2
        backgroundPath.stroke()
        
        switch phase {
        case .Fokusphase:
            NSColor.systemRed.set()
        case .Verfügbar:
            NSColor.systemGreen.set()
        case .None:
            break
        }
        
        var text = "\(remainingTime)\n\(phase.rawValue)"

        if phase != .None {
            let endAngle = ((Float  (remainingSecs) * 0.1)) + 90

            let timerPath = NSBezierPath()
            timerPath.appendArc(withCenter: center, radius: rect.width / 2 - 4, startAngle: 90, endAngle: CGFloat(endAngle), clockwise: false)
            timerPath.lineWidth = 8
            timerPath.lineCapStyle = .round
            timerPath.stroke()
        } else {
            text = remainingTime           
        }

        let remainingTimeFont = NSFont.systemFont(ofSize: CGFloat(28))
        let phaseFont = NSFont.systemFont(ofSize: CGFloat(12))

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center

        let remainingTimeRange = (text as NSString).range(of: remainingTime)
        let remainingTimeString = NSMutableAttributedString(string: text)

        let phaseRange = (text as NSString).range(of: phase.rawValue)

        let textRange = (text as NSString).range(of: text)

        remainingTimeString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: textRange)
        remainingTimeString.addAttribute(NSAttributedString.Key.font, value: remainingTimeFont, range: remainingTimeRange)
        remainingTimeString.addAttribute(NSAttributedString.Key.font, value: phaseFont, range: phaseRange)
        remainingTimeString.addAttribute(NSAttributedString.Key.foregroundColor, value: NSColor.white, range: textRange)

        let constrainRect = CGSize(width: CGFloat(rect.width), height: CGFloat(rect.height))
        let boundingBox = remainingTimeString.boundingRect(with: constrainRect, options: [.usesLineFragmentOrigin, .usesFontLeading])

        let y = CGFloat(center.y - (boundingBox.height / 2))

        remainingTimeString.draw(with: CGRect(x: CGFloat(0), y: y, width: rect.width, height: boundingBox.height), options: [.usesLineFragmentOrigin,  .usesFontLeading], context: nil)
        
    }
    
}
